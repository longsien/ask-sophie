let elementsToAnimate = document.querySelectorAll('.animate')

function animate () {
  Array.prototype.forEach.call(elementsToAnimate, (el, i) => {
    el.querySelectorAll('.animate-child').length && animateChildren(el)
    el.classList.add('on')
  })
}

function animateChildren (el) {
  let childrenToAnimate = el.querySelectorAll('.animate-child')

  Array.prototype.forEach.call(childrenToAnimate, (child, j) => {
    setTimeout(() => {
      child.classList.add('on')}, j * 300)
  })
}

(function () {
  animate()
})()
