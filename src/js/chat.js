import { ApiAiClient, ApiAiStreamClient } from 'api-ai-javascript'

const client = new ApiAiClient({accessToken: '5e711eea9b9e4cd78a9834d8864338d4', streamClientClass: ApiAiStreamClient})

let chatbox = document.getElementById('chat')
let responseContainer = document.getElementById('response-container')
let responseBox = document.getElementById('response')
let submitButton = document.getElementById('submit')
let loadingBubble = document.getElementById('loading-container')

chatbox.addEventListener('keyup', enterKey, { passive: true })
submitButton.addEventListener('click', chatInput, false)

function enterKey (e) {
  if (e.keyCode == 13) {
    chatInput()
  }
}

function chatInput () {
  chat(chatbox.value)
  response(chatbox.value, 'user')

  chatbox.value = ''

  setTimeout(showBubble(true), 200)
}

function showBubble (state) {
  state ? loadingBubble.classList.add('show') : loadingBubble.classList.remove('show')
  scrollToBottom()
}

function chat (txt) {
  client.textRequest(txt)
    .then((data) => {
      botResponse(data.result.fulfillment.speech)
    })
    .catch((error) => {
      console.log(error)
    })
}

function botResponse (txt) {
  let time = txt.length * 10

  setTimeout(() => response(txt, 'bot'), time)
}

function response (txt, sender) {
  let lastChild = document.querySelector(`.${sender}:last-child`)
  let el = document.createElement('div')

  if (lastChild) {
    el.innerHTML = `<span>${txt}</span>`
    lastChild.appendChild(el)
  } else {
    el.classList.add(sender)
    el.innerHTML = `<div><span>${txt}</span><div>`
    responseBox.appendChild(el)
  }

  showBubble(false)

  scrollToBottom()
}

function scrollToBottom () {
  responseContainer.scrollTop = responseContainer.scrollHeight
}
